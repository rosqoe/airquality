//
//  Bundle+Data.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 14/02/2022.
//

import Foundation

extension Bundle {

    /**
    Extract data to a json file
    
    - parameter resource: name of the json file.
    - returns: data
    */
    func dataFromJson(_ name: String) -> Data {
        guard let mockURL = url(forResource: name, withExtension: "json"),
              let data = try? Data(contentsOf: mockURL) else {
                 fatalError("Failed to load \(name) from bundle.")
        }
        return data
    }
}
