//
//  FakeResponseData.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 14/02/2022.
//

import Foundation

final class FakeResponseData {

    // MARK: - URL

    static let url: URL = URL(string: "http://api.openweathermap.org/data/2.5/air_pollution?lat=47.59676375465199&lon=1.6675560697923117&appid=97b1010ae578d3da9df3c5340a27c705")!

    // MARK: - Responses

    static let validResponse = HTTPURLResponse(url: URL(string: "https://www.apple.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)!
    static let invalidResponse = HTTPURLResponse(url: URL(string: "https://www.apple.com")!, statusCode: 500, httpVersion: nil, headerFields: nil)!

    // MARK: - Data

    static var correctData: Data {
        let bundle = Bundle(for: FakeResponseData.self)
        return bundle.dataFromJson("AirPollutionData")
    }
    
    static let incorrectData = "erreur".data(using: .utf8)!
}
