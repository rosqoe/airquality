//
//  AirPollutionServiceStub.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 14/02/2022.
//

import Foundation
@testable import AirQuality

struct AirPollutionServiceStub: AirPollutionServiceType {

    // MARK: - Properties

    private let isSuccess: Bool

    // MARK: - Initializer

    init(isSuccess: Bool) {
        self.isSuccess = isSuccess
    }

    // MARK: - Management

    func request(coordinate: Coordinate) async throws -> AirDataResponse {
        if isSuccess {
            return .stub
        } else {
            throw APIError.invalidResponse
        }
    }
}

extension AirDataResponse {
    static let stub: Self = .init(aqi: 1,
                                  lat: 47.5968,
                                  lon: 1.6676,
                                  dt: 1644840000,
                                  components: [
                                    "co": 240.33,
                                    "no": 0.31,
                                    "no2": 1.39,
                                    "o3": 64.37,
                                    "so2": 0.46,
                                    "pm2_5": 1.4,
                                    "pm10": 1.91,
                                    "nh3": 0.87
                                  ])
}
