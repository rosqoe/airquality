//
//  AirPollutionViewModelTests.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 14/02/2022.
//

import XCTest
@testable import AirQuality

final class AirPollutionViewModelTests: XCTestCase {

    // MARK: - Properties

    private let coordinate: Coordinate = .init(latitude: 47.59681243564, longitude: 1.66761256456745)

    // MARK: - Tests

    func test_GivenLoadAirPollutionDataMethod_WhenServiceGetAGoodData_ThenShouldReturnTheCorrectResult() async {
        let service: AirPollutionServiceType = AirPollutionServiceStub(isSuccess: true)
        let sut: AirPollutionViewModel = .init(service: service)
        sut.coordinates = [coordinate]
        await sut.loadAirPollutionData()

        XCTAssertTrue(sut.air.quality.name == "Bon")
        XCTAssertTrue(sut.air.lat == "47.59680")
        XCTAssertTrue(sut.air.lon == "1.66760")
        XCTAssertTrue(sut.air.dt == "14 févr. 2022 à 1:00 PM")
        XCTAssertTrue(!sut.air.components.isEmpty)
        XCTAssertTrue(sut.air.components.count == 4)
        XCTAssertTrue(sut.air.components[0].name == "Dioxyde d'azote")
        XCTAssertTrue(sut.air.components[1].name == "Ozone")
        XCTAssertTrue(sut.air.components[2].name == "Particules en suspension")
        XCTAssertTrue(sut.air.components[3].name == "Particules fines")
        XCTAssertTrue(sut.air.components[0].color == .green)
        XCTAssertTrue(sut.air.components[0].value == "1.39")
    }

    func test_GivenLoadAirPollutionDataMethod_WhenServiceThrowAnErro_ThenShouldReturnTheCorrectError() async {
        let service: AirPollutionServiceType = AirPollutionServiceStub(isSuccess: false)
        let sut: AirPollutionViewModel = .init(service: service)
        sut.coordinates = [coordinate]
        await sut.loadAirPollutionData()

        XCTAssertTrue(sut.error?.localizedDescription == "Air pollution unaivailable")
    }
}
