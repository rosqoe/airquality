//
//  AirPlacesViewModelTests.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 16/03/2022.
//

import XCTest
@testable import AirQuality

final class AirPlacesViewModelTests: XCTestCase {

    // MARK: - Tests

    func test_GivenLoadAirPollutionDataMethod_WhenServiceGetAGoodData_ThenShouldReturnTheCorrectResult() async {
        let service: AirPollutionServiceType = AirPollutionServiceStub(isSuccess: true)
        let sut: AirPlacesViewModel = .init(service: service)
        await sut.loadAirPollutionData()

        let firstPlace = sut.airPlaces.first
        XCTAssertTrue(firstPlace!.name == "Amsterdam")
        XCTAssertTrue(firstPlace!.quality == "Bon")
        XCTAssertTrue(firstPlace!.color == .green)
        
        let lastPlace = sut.airPlaces.last
        XCTAssertTrue(lastPlace!.name == "Zagreb")
        XCTAssertTrue(lastPlace!.quality == "Bon")
        XCTAssertTrue(lastPlace!.color == .green)
    }

    func test_GivenLoadAirPollutionDataMethod_WhenServiceThrowAnErro_ThenShouldReturnTheCorrectError() async {
        let service: AirPollutionServiceType = AirPollutionServiceStub(isSuccess: false)
        let sut: AirPlacesViewModel = .init(service: service)
        await sut.loadAirPollutionData()

        XCTAssertTrue(sut.error?.localizedDescription == "Air pollution unaivailable")
    }
}
