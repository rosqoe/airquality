//
//  AirPollutionServiceTests.swift
//  AirQualityTests
//
//  Created by Sebastien Bastide on 13/02/2022.
//

import XCTest
@testable import AirQuality

final class AirPollutionServiceTests: XCTestCase {

    // MARK: - Properties

    private let sessionConfiguration: URLSessionConfiguration = {
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        sessionConfiguration.protocolClasses = [URLProtocolFake.self]
        return sessionConfiguration
    }()
    let coordinate: Coordinate = .init(latitude: 47.59676375465199, longitude: 1.6675560697923117)

    // MARK: - Tests

    func test_GivenRequestMethod_WhenIncorrectResponseIsPassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (nil, FakeResponseData.invalidResponse)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: AirPollutionService = .init(session: fakeSession)
        
        do {
            let _: AirDataResponse = try await sut.request(coordinate: coordinate)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Air pollution unaivailable")
        }
    }

    func test_GivenRequestMethod_WhenIncorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectError() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.incorrectData,
                                                           FakeResponseData.validResponse)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: AirPollutionService = .init(session: fakeSession)
        
        do {
            let _: AirDataResponse = try await sut.request(coordinate: coordinate)
            XCTFail(#function, file: #file, line: #line)
        } catch {
            XCTAssertEqual(error.localizedDescription, "Air pollution unaivailable")
        }
    }

    func test_GivenRequestMethod_WhenCorrectDataAndGoodResponseArePassed_ThenShouldReturnTheCorrectResult() async {
        URLProtocolFake.fakeURLs = [FakeResponseData.url: (FakeResponseData.correctData,
                                                           FakeResponseData.validResponse)]
        let fakeSession = URLSession(configuration: sessionConfiguration)
        let sut: AirPollutionService = .init(session: fakeSession)
        
        do {
            let airPollutionData: AirDataResponse = try await sut.request(coordinate: coordinate)
            XCTAssertTrue(airPollutionData.dt == 1644840000)
            XCTAssertTrue(airPollutionData.aqi == 1)
        } catch {
            XCTFail(#function, file: #file, line: #line)
        }
    }
}
