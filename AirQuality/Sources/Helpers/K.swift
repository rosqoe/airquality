//
//  K.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 16/03/2022.
//

import Foundation

struct K {

    // MARK: - Icon

    struct Icon {
        static let mapIcon: String = "map.fill"
        static let mappinCircle: String = "mappin.circle.fill"
    }

    // MARK: - AirPollution

    struct AirPollution {
        static let navigationBarTitle: String = "AirPollutionNavigationBarTitle".localized
    }

    // MARK: - BottomSheet

    struct BottomSheet {
        static let titleLongPress: String = "BottomSheetLongPressTitle".localized
        static let messageLongPress: String = "BottomSheetLongPressMessage".localized
    }

    // MARK: - AirQuality

    struct AirQuality {
        static let titleGood: String = "AirQualityGoodTitle".localized
        static let titleModeratelyPolluted: String = "AirQualityModeratelyPollutedTitle".localized
        static let titleSevere: String = "AirQualitySevereTitle".localized
    }

    // MARK: - Pollutant

    struct Pollutant {
        static let titleCoarseParticulate: String = "CoarseParticulateTitle".localized
        static let titleFineParticles: String = "FineParticlesTitle".localized
        static let titleNitrogenDioxide: String = "NitrogenDioxideTitle".localized
        static let titleOzone: String = "OzoneTitle".localized
    }

    // MARK: - AirPlaces

    struct AirPlaces {
        static let navigationBarTitle: String = "AirPlacesNavigationBarTitle".localized
    }

    // MARK: - Alert

    struct Alert {
        // Titles
        static let titleError: String = "AlertErrorTitle".localized
        static let titleOk: String = "AlertOkTitle".localized
    }

    // MARK: - Cities

    struct Cities {
        static let berlin: String = "Berlin".localized
        static let vienna: String = "Vienna".localized
        static let brussels: String = "Brussels".localized
        static let sofia: String = "Sofia".localized
        static let nicosia: String = "Nicosia".localized
        static let zagreb: String = "Zagreb".localized
        static let copenhagen: String = "Copenhagen".localized
        static let madrid: String = "Madrid".localized
        static let tallinn: String = "Tallinn".localized
        static let helsinki: String = "Helsinki".localized
        static let paris: String = "Paris".localized
        static let athens: String = "Athens".localized
        static let budapest: String = "Budapest".localized
        static let dublin: String = "Dublin".localized
        static let rome: String = "Rome".localized
        static let riga: String = "Riga".localized
        static let vilnius: String = "Vilnius".localized
        static let luxembourg: String = "Luxembourg".localized
        static let amsterdam: String = "Amsterdam".localized
        static let warsaw: String = "Warsaw".localized
        static let lisbon: String = "Lisbon".localized
        static let bucharest: String = "Bucharest".localized
        static let bratislava: String = "Bratislava".localized
        static let ljubljana: String = "Ljubljana".localized
        static let stockholm: String = "Stockholm".localized
        static let prague: String = "Prague".localized
    }
}
