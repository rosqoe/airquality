//
//  String+Localizable.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 16/03/2022.
//

import Foundation

extension String {
    /**
     Convert a String to a LocalizedString.

     - returns: String.
     */
    var localized: String {
        NSLocalizedString(self, comment: .init())
    }
}
