//
//  ProcessInfo+RunningTests.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 13/02/2022.
//

import Foundation

extension ProcessInfo {
    var isRunningTests: Bool {
        environment["XCTestConfigurationFilePath"] != nil
    }
}
