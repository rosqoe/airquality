//
//  Color+Air.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import SwiftUI

extension Color {

    // MARK: - Initializer

    /**
     Easy way to build a custom color.
     
     - parameter r: red value.
     - parameter g: green value.
     - parameter b: blue value.
     - returns: UIColor
     */
    init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(.sRGB, red: r / 255, green: g / 255, blue: b / 255, opacity: 1)
    }

    // MARK: - Colors

    /**
     Determined the index color of a pollutant
     
     - parameter airColor: Air.Color
     - returns: Color
     */
    static func pollutantColor(to airColor: Air.Color) -> Color {
        switch airColor {
        case .green:
            return .init(r: 136, g: 186, b: 115)
        case .yellow:
            return .init(r: 231, g: 196, b: 67)
        case .red:
            return .init(r: 214, g: 79, b: 112)
        case .black: return .black
        }
    }
}
