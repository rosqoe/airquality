//
//  Double+FormattingData.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import Foundation

extension Double {

    /**
     Convert the double value to a String with five decimals.

     - returns: String
     */
    var mapCoordinatFormat: String {
        String(format: "%.5f", self)
    }

    /**
     Convert the double value to a String with two decimals.

     - returns: String
     */
    var pollutantFormat: String {
        String(format: "%.2f", self)
    }

    /**
     Determined the index color of a pollutant based on pollutant index
     
     - parameter pollutant: String
     - returns: Air.Color
     */
    func color(for pollutant: String) -> Air.Color {
        switch pollutant {
        case "pm10": return self.coarseParticulesColor
        case "pm2_5": return self.fineParticlesColor
        case "no2": return self.nitrogenDioxideColor
        case "o3": return self.ozoneColor
        default: return .black
        }
    }

    /**
     Determined the index color of a coarse particules pollutant

     - returns: Air.Color
     */
    private var coarseParticulesColor: Air.Color {
        switch self {
        case 0 ... 50: return .green
        case 50 ... 90: return .yellow
        case let value where value > 90: return .red
        default: return .black
        }
    }

    /**
     Determined the index color of a fine particules pollutant

     - returns: Air.Color
     */
    private var fineParticlesColor: Air.Color {
        switch self {
        case 0 ... 30: return .green
        case 30 ... 55: return .yellow
        case let value where value > 55: return .red
        default: return .black
        }
    }

    /**
     Determined the index color of a nitrogen dioxide particules pollutant

     - returns: Air.Color
     */
    private var nitrogenDioxideColor: Air.Color {
        switch self {
        case 0 ... 100: return .green
        case 100 ... 200: return .yellow
        case let value where value > 200: return .red
        default: return .black
        }
    }

    /**
     Determined the index color of an ozone pollutant

     - returns: Air.Color
     */
    private var ozoneColor: Air.Color {
        switch self {
        case 0 ... 120: return .green
        case 120 ... 180: return .yellow
        case let value where value > 180: return .red
        default: return .black
        }
    }
}
