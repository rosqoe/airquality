//
//  Dictionary:FormattingData.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import Foundation

extension Dictionary where Key == String, Value == Double {

    /**
     Sort pollutants to a specific list and convert them to an Air.Component list

     - returns: [Air.Component]
     */
    var pollutantComponents: [Air.Component] {
        var components: [Air.Component] = []
        let pollutantSelections: [String] = ["pm10", "pm2_5", "no2", "o3"]
        for (key, value) in self.sorted(by: <) {
            if pollutantSelections.contains(key) {
                components.append(Air.Component(name: key.pollutantName,
                                                value: value.pollutantFormat,
                                                color: value.color(for: key)))
            }
        }
        return components
    }
}
