//
//  Int+FormattingData.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import Foundation

extension Int {

    /**
     Determined the air quality

     - returns: Air.Quality
     */
    var quality: Air.Quality {
        Air.Quality(name: self.qualityName, color: self.qualityColor)
    }

    /**
     Transform a timestamp to a string date

     - returns: String
     */
    var stringDateFormat: String {
        let date = Date(timeIntervalSince1970: .init(self))
        let formatter: DateFormatter = .init()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }

    // MARK: - Helpers

    /**
     Determined the quality name based on air quality index

     - returns: String
     */
    var qualityName: String {
        switch self {
        case 1 ... 2: return K.AirQuality.titleGood
        case 3: return K.AirQuality.titleModeratelyPolluted
        case 4 ... 5: return K.AirQuality.titleSevere
        default: return .init()
        }
    }

    /**
     Determined the quality color based on air quality index

     - returns: Air.Color
     */
    var qualityColor: Air.Color {
        switch self {
        case 1 ... 2: return .green
        case 3: return .yellow
        case 4 ... 5: return .red
        default: return .black
        }
    }
}
