//
//  String+FormattingData.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import Foundation

extension String {

    /**
     Transform the pollutant index to a pollutant name.
     
     - returns: String
     */
    var pollutantName: Self {
        switch self {
        case "pm10": return K.Pollutant.titleCoarseParticulate
        case "pm2_5": return K.Pollutant.titleFineParticles
        case "no2": return K.Pollutant.titleNitrogenDioxide
        case "o3": return K.Pollutant.titleOzone
        default: return .init()
        }
    }
}
