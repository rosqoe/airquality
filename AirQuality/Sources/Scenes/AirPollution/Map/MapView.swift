//
//  MapView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import SwiftUI

struct MapView: UIViewControllerRepresentable {

    // MARK: - Properties

    let viewModel: AirPollutionViewModel
    
    // MARK: - Initializer

    func makeUIViewController(context: Context) -> MapViewController {
        let mapViewController = MapViewController()
        mapViewController.viewModel = viewModel
        return mapViewController
    }

    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {}
}
