//
//  MapViewController.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import MapKit

final class MapViewController: UIViewController {

    // MARK: - Properties

    private let mapView: MKMapView = .init(frame: UIScreen.main.bounds)
    var viewModel: AirPollutionViewModel?
    var annotations: [MKPointAnnotation] = []

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        mapView.delegate = self
        let longTapGesture = UILongPressGestureRecognizer(target: self,
                                                          action: #selector(longTap(sender:)))
        mapView.addGestureRecognizer(longTapGesture)
    }

    // MARK: - Management

    @objc
    private func longTap(sender: UIGestureRecognizer) {
        if sender.state == .began {
            let locationInView = sender.location(in: mapView)
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            if !annotations.isEmpty { mapView.removeAnnotation(annotations.last!) }
            viewModel?.coordinates.removeAll()
            addAnnotation(location: locationOnMap)
        }
    }
    
    private func addAnnotation(location: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotations.append(annotation)
        viewModel?.coordinates.append(.init(latitude: location.latitude, longitude: location.longitude))
        mapView.addAnnotation(annotation)
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        Task { await viewModel?.loadAirPollutionData() }
    }
}
