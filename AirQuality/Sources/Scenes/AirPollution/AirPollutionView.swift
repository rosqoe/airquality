//
//  AirPollutionView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 10/02/2022.
//

import SwiftUI
import MapKit

struct AirPollutionView: View {

    // MARK: - Properties

    @State private var isBottomSheetOpen: Bool = false
    @State private var isAirPlacesOpen: Bool = false
    @StateObject var viewModel: AirPollutionViewModel

    // MARK: - Main View

    var body: some View {
        GeometryReader { _ in
            MainNavigationView(title: K.AirPollution.navigationBarTitle, mapAction: { isAirPlacesOpen.toggle() }) {
                MapView(viewModel: viewModel)
                    .navigationBarTitleDisplayMode(.inline)
            }
            BottomSheetView(
                isOpen: $isBottomSheetOpen,
                maxHeight: 380
            ) {
                BottomSheetContentView(air: viewModel.air)
            }
        }
        .edgesIgnoringSafeArea(.all)
        .alert(isPresented: Binding<Bool>.constant($viewModel.error.wrappedValue != nil)) { alert }
        .sheet(isPresented: $isAirPlacesOpen) { AirPlacesView(viewModel: .init()) }
    }

    // MARK: - Alert

    private var alert: Alert {
        Alert(title: .init(K.Alert.titleError),
              message: .init($viewModel.error.wrappedValue?.localizedDescription ?? .init()),
              dismissButton: .default(.init(K.Alert.titleOk), action: {
            $viewModel.error.wrappedValue = nil
        }))
    }
}

#if DEBUG
struct AirPollutionView_Previews: PreviewProvider {
    static var previews: some View {
        AirPollutionView(viewModel: .init())
    }
}
#endif
