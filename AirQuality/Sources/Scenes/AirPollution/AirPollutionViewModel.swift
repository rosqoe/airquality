//
//  AirPollutionViewModel.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import Foundation

final class AirPollutionViewModel: ObservableObject {

    // MARK: - Properties

    @Published private(set) var air: Air = .initial
    @Published var error: Error?
    private let service: AirPollutionServiceType
    var coordinates: [Coordinate] = []

    // MARK: - Initializer

    init(service: AirPollutionServiceType = AirPollutionService()) {
        self.service = service
    }

    // MARK: - Management

    @MainActor
    func loadAirPollutionData() async {
        do {
            guard let coordinate = coordinates.first else { return }
            let data = try await service.request(coordinate: coordinate)
            self.air = Air(data: data)
        } catch {
            self.error = error
        }
    }
}
