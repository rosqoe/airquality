//
//  BottomSheetContentView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 13/02/2022.
//

import SwiftUI

struct BottomSheetContentView: View {

    // MARK: - Properties

    let air: Air

    // MARK: - Main View

    var body: some View {
        air.isActive ? AnyView(annotationView) : AnyView(noAnnotationView)
    }

    // MARK: - Views

    private var noAnnotationView: some View {
        VStack(spacing: 10) {
            Spacer()
            Image(systemName: K.Icon.mappinCircle).font(.system(size: 40, weight: .bold))
            Text(K.BottomSheet.titleLongPress).font(.system(size: 20, weight: .bold))
            Text(K.BottomSheet.messageLongPress).font(.body)
            Spacer()
        }
        .offset(y: -15)
        .foregroundColor(Color.gray)
        .padding()
    }

    private var annotationView: some View {
        VStack(spacing: 5) {
            summaryView.padding()
            listOfDetails.padding(.horizontal)
        }
    }

    private var summaryView: some View {
        VStack(spacing: 5) {
            HStack {
                Text(air.quality.name)
                    .font(.title)
                    .foregroundColor(Color.pollutantColor(to: air.quality.color))
                Spacer()
            }.padding(.horizontal)
            HStack {
                Text("Lat: \(air.lat)").font(.caption2)
                Text("Lon: \(air.lon)").font(.caption2)
                Spacer()
            }.padding(.horizontal)
            HStack {
                Text("\(air.dt)").font(.caption2)
                Spacer()
            }.padding(.horizontal)
        }
        .padding()
        .background(Color.white)
        .cornerRadius(10)
    }

    private var listOfDetails: some View {
        List(air.components, id: \.id) { component in
            HStack {
                Text("\(component.name)")
                Spacer()
                Text("\(component.value) ").foregroundColor(Color.pollutantColor(to: component.color)).fontWeight(.bold) + Text("*μg/m3*")
            }
        }
        .listStyle(PlainListStyle())
    }
}

#if DEBUG
struct BottomSheetContentView_Previews: PreviewProvider {
    static var previews: some View {
        BottomSheetContentView(air: .initial)
    }
}
#endif
