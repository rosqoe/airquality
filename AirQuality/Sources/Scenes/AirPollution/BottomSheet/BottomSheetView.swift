//
//  BottomSheetView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import SwiftUI

struct BottomSheetView<Content: View>: View {

    // MARK: - Properties

    @Binding var isOpen: Bool
    @GestureState private var translation: CGFloat = 0

    let maxHeight: CGFloat
    let minHeight: CGFloat
    let content: Content

    private var offset: CGFloat {
        isOpen ? 0 : maxHeight - minHeight
    }

    // MARK: - Initializer

    init(isOpen: Binding<Bool>, maxHeight: CGFloat, @ViewBuilder content: () -> Content) {
        self._isOpen = isOpen
        self.maxHeight = maxHeight
        self.minHeight = maxHeight * 0.14
        self.content = content()
    }

    // MARK: - Main View

    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                self.indicator.padding(5)
                self.content
            }
            .frame(width: geometry.size.width, height: maxHeight, alignment: .top)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(16)
            .frame(height: geometry.size.height, alignment: .bottom)
            .offset(y: max(offset + translation, 0))
            .animation(.interactiveSpring(), value: translation)
            .gesture(
                DragGesture().updating($translation) { value, state, _ in
                    state = value.translation.height
                }.onEnded { value in
                    let snapDistance = maxHeight * 0.25
                    guard abs(value.translation.height) > snapDistance else { return }
                    isOpen = value.translation.height < 0
                }
            )
        }
    }

    // MARK: - Views

    private var indicator: some View {
        RoundedRectangle(cornerRadius: 16)
            .fill(Color.secondary)
            .frame(width: 40, height: 6)
            .onTapGesture {
                self.isOpen.toggle()
            }
    }
}

struct BottomSheetView_Previews: PreviewProvider {
    static var previews: some View {
        BottomSheetView(isOpen: .constant(false), maxHeight: 600) {
            Rectangle().fill(Color.red)
        }.edgesIgnoringSafeArea(.all)
    }
}
