//
//  AirPlacesView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 14/03/2022.
//

import SwiftUI

struct AirPlacesView: View {

    // MARK: - Properties

    @StateObject var viewModel: AirPlacesViewModel

    // MARK: - Main View

    var body: some View {
        NavigationView {
            content
                .navigationBarTitle(K.AirPlaces.navigationBarTitle, displayMode: .inline)
        }
        .onAppear { Task { await viewModel.loadAirPollutionData() } }
        .alert(isPresented: Binding<Bool>.constant($viewModel.error.wrappedValue != nil)) { alert }
    }

    // MARK: - Views

    private var content: some View {
        return !viewModel.isLoading ? AnyView(favoritesList) : AnyView(ProgressView())
    }

    private var favoritesList: some View {
        List(viewModel.airPlaces) { airPlace in
            HStack {
                Text("\(airPlace.name)")
                Spacer()
                Text("\(airPlace.quality) ")
                    .foregroundColor(Color.pollutantColor(to: airPlace.color))
                    .fontWeight(.bold)
            }
        }
        .cornerRadius(20)
    }

    // MARK: - Alert

    private var alert: Alert {
        Alert(title: .init(K.Alert.titleError),
              message: .init($viewModel.error.wrappedValue?.localizedDescription ?? .init()),
              dismissButton: .default(.init(K.Alert.titleOk), action: {
            $viewModel.error.wrappedValue = nil
        }))
    }
}

#if DEBUG
struct AirPlacesView_Previews: PreviewProvider {
    static var previews: some View {
        AirPlacesView(viewModel: .init())
    }
}
#endif
