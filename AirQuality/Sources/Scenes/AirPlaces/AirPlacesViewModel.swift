//
//  AirPlacesViewModel.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 14/03/2022.
//

import Foundation

final class AirPlacesViewModel: ObservableObject {

    // MARK: - Properties

    @Published private(set) var airPlaces: [AirPlace] = []
    @Published private(set) var isLoading: Bool = false
    @Published var error: Error?
    private let service: AirPollutionServiceType
    private let euroCapitalCoordinates: [String: (Double, Double)] = [
        K.Cities.berlin: (52.5167, 13.3833), K.Cities.vienna: (48.2083, 16.3731), K.Cities.brussels: (50.8333, 4.3333), K.Cities.sofia: (42.6979, 23.3217), K.Cities.nicosia: (35.1725, 33.3650), K.Cities.zagreb: (45.8000, 15.9500), K.Cities.copenhagen: (55.6761, 12.5689), K.Cities.madrid: (40.4167, -3.7167), K.Cities.tallinn: (59.4372, 24.7450), K.Cities.helsinki: (60.1756, 24.9342), K.Cities.paris: (48.8566, 2.3522), K.Cities.athens: (37.9842, 23.7281), K.Cities.budapest: (47.4983, 19.0408), K.Cities.dublin: (53.3497, -6.2603), K.Cities.rome: (41.8931, 12.4828), K.Cities.riga: (56.9475, 24.1069), K.Cities.vilnius: (54.6833, 25.2833), K.Cities.luxembourg: (49.6106, 6.1328), K.Cities.amsterdam: (52.3667, 4.8833), K.Cities.warsaw: (52.2167, 21.0333), K.Cities.lisbon: (38.7452, -9.1604), K.Cities.bucharest: (44.4000, 26.0833), K.Cities.bratislava: (48.1447, 17.1128), K.Cities.ljubljana: (46.0500, 14.5167), K.Cities.stockholm: (59.3294, 18.0686), K.Cities.prague: (50.0833, 14.4167)
    ]

    // MARK: - Initializer

    init(service: AirPollutionServiceType = AirPollutionService()) {
        self.service = service
    }

    // MARK: - Management

    @MainActor
    func loadAirPollutionData() async {
        isLoading = true
        var airPlaces: [AirPlace] = []
        for (key, value) in euroCapitalCoordinates {
            do {
                let data = try await service.request(coordinate: .init(latitude: value.0, longitude: value.1))
                let airPlace: AirPlace = .init(name: key,
                                               quality: data.aqi.qualityName,
                                               color: data.aqi.qualityColor)
                airPlaces.append(airPlace)
            } catch {
                self.error = error
            }
        }
        self.airPlaces = airPlaces.sorted(by: { $0.name < $1.name })
        isLoading = false
    }
}
