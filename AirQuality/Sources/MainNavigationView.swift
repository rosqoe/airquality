//
//  MainNavigationView.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 14/03/2022.
//

import SwiftUI

struct MainNavigationView<Content: View>: View {

    // MARK: - Properties

    let title: String
    let mapAction: () -> Void
    let content: Content

    // MARK: - Initializer

    init(title: String,
         mapAction: @escaping () -> Void,
         @ViewBuilder content: () -> Content) {
        self.title = title
        self.mapAction = mapAction
        self.content = content()
    }

    // MARK: - Main View

    var body: some View {
        NavigationView {
            content
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Text(title).font(.title).fontWeight(.bold)
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button(action: mapAction) {
                            Image(systemName: K.Icon.mapIcon).foregroundColor(.black)
                        }
                    }
                }
        }
    }
}

#if DEBUG
struct MainNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        MainNavigationView(title: "AirPollution",
                           mapAction: {},
                           content: { Text("Hello world") }
        )
    }
}
#endif
