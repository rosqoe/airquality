//
//  AirPollutionService.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import Foundation

protocol AirPollutionServiceType {
    func request(coordinate: Coordinate) async throws -> AirDataResponse
}

struct AirPollutionService: AirPollutionServiceType {

    // MARK: - Properties

    private let session: URLSession
    private let decoder: JSONDecoder = .init()

    // MARK: - Initializer

    init(session: URLSession = .init(configuration: .default)) {
        self.session = session
    }

    // MARK: - Management

    func request(coordinate: Coordinate) async throws -> AirDataResponse {
        let endpoint = Endpoint.currentAirPollution(for: coordinate)
        let (data, response) = try await session.data(from: endpoint.url)
        try responseHandler(with: response)
        return try dataHandler(with: data)
    }

    private func responseHandler(with response: URLResponse) throws {
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw APIError.invalidResponse
        }
    }

    private func dataHandler(with data: Data) throws -> AirDataResponse {
        guard let dataDecoded = try? decoder.decode(AirDataResponse.self, from: data) else {
            throw APIError.undecodableData
        }
        return dataDecoded
    }
}
