//
//  Endpoint.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 13/02/2022.
//

import Foundation

struct Endpoint {
    var path: String
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "api.openweathermap.org"
        components.path = "/data/2.5/" + path
        components.queryItems = queryItems

        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }

        return url
    }
}

extension Endpoint {
    static func currentAirPollution(for coordinate: Coordinate) -> Self {
            Endpoint(
                path: "air_pollution",
                queryItems: [
                    .init(name: "lat", value: "\(coordinate.latitude)"),
                    .init(name: "lon", value: "\(coordinate.longitude)"),
                    .init(name: "appid", value: "97b1010ae578d3da9df3c5340a27c705")
                ]
            )
        }
}
