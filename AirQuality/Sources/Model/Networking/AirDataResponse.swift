//
//  AirDataResponse.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import Foundation

struct AirDataResponse: Decodable {
    let aqi: Int
    let lat: Double
    let lon: Double
    let dt: Int
    let components: [String: Double]

    enum CodingKeys: String, CodingKey {
        case coord, list
    }

    struct Coord: Decodable {
        let lon, lat: Double
    }

    struct List: Decodable {
        let main: Main
        let components: [String: Double]
        let dt: Int

        struct Main: Decodable {
            let aqi: Int
        }
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let coord = try container.decode(Coord.self, forKey: .coord)
        let list = try container.decode([List].self, forKey: .list)

        self.aqi = list[0].main.aqi
        self.lat = coord.lat
        self.lon = coord.lon
        self.dt = list[0].dt
        self.components = list[0].components
    }
}

extension AirDataResponse {
    init(aqi: Int, lat: Double, lon: Double, dt: Int, components: [String: Double]) {
        self.aqi = aqi
        self.lat = lat
        self.lon = lon
        self.dt = dt
        self.components = components
    }
}
