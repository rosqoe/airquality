//
//  APIError.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 11/02/2022.
//

import Foundation

enum APIError: Error {
    case invalidResponse
    case undecodableData
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidResponse:
            return NSLocalizedString("Air pollution unaivailable", comment: .init())
        case .undecodableData:
            return NSLocalizedString("Air pollution unaivailable", comment: .init())
        }
    }
}
