//
//  Air.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 12/02/2022.
//

import Foundation

struct Air {
    let isActive: Bool
    let quality: Quality
    let lat: String
    let lon: String
    let dt: String
    let components: [Component]

    struct Quality {
        let name: String
        let color: Air.Color
    }

    struct Component: Identifiable {
        let id: UUID = .init()
        let name: String
        let value: String
        let color: Air.Color
    }

    enum Color {
        case green, yellow, red, black
    }
}

extension Air {
    private init() {
        self.isActive = false
        self.quality = .init(name: .init(), color: .black)
        self.lat = .init()
        self.lon = .init()
        self.dt = .init()
        self.components = []
    }

    init(data: AirDataResponse) {
        self.isActive = true
        self.quality = data.aqi.quality
        self.lat = data.lat.mapCoordinatFormat
        self.lon = data.lon.mapCoordinatFormat
        self.dt = data.dt.stringDateFormat
        self.components = data.components.pollutantComponents
    }
}

extension Air {
    static let initial: Self = .init()
}
