//
//  AirPlace.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 14/03/2022.
//

import Foundation

struct AirPlace: Identifiable {
    let id: UUID = .init()
    let name: String
    let quality: String
    let color: Air.Color
}
