//
//  Coordinate.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 10/02/2022.
//

import Foundation

struct Coordinate {
    let latitude: Double
    let longitude: Double
}
