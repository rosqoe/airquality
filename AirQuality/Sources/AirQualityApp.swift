//
//  AirQualityApp.swift
//  AirQuality
//
//  Created by Sebastien Bastide on 10/02/2022.
//

import SwiftUI

@main
struct AirQualityApp: App {

    // MARK: - Properties

    private let isRunningTests: Bool = ProcessInfo.processInfo.isRunningTests

    // MARK: - Main Scene

    var body: some Scene {
        WindowGroup {
            if !isRunningTests {
                AirPollutionView(viewModel: .init())
            } else {
                Text("Units tests are running...")
            }
        }
    }
}
